const express = require('express')
const users = express.Router()
const cors = require('cors')
const jwt = require('jsonwebtoken')

const User = require('../models/User')
const Chil = require('../models/child')

process.env.SECRET_KEY = 'secret'
var especialid;

users.post('/register', (req, res) => {
    const childData = {
      username: req.body.username,
      email: req.body.email,
      password: req.body.password
    }
    User.findOne({
    username: req.body.username
    })
      //TODO bcrypt
      .then(child => {
        if (!child) {
          Child.create(childData)
            .then(child => {
              const payload = {
                _id: child._id,
                username: child.username,
                email: child.email
              }
              let token = jwt.sign(payload, process.env.SECRET_KEY, {
                expiresIn: 1440
              })
              res.json({ token: token })
            })
            .catch(err => {
              res.send('error: ' + err)
            })
        } else {
          res.json({ error: 'child already exists' })
        }
      })
      .catch(err => {
        res.send('error: ' + err)
      })
  })