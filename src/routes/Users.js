const express = require('express')
const users = express.Router()
const cors = require('cors')
const jwt = require('jsonwebtoken')

const User = require('../models/User')

process.env.SECRET_KEY = 'secret'
var especialid;
var especialcode;

users.put('/verifyAccount', (req, res) => {
  
  User.findOne({
    _id: especialid
  })
    .then(user => {
      if (user.code == especialcode) {
        user.isVerified = true
        user.save()
        res.status(200).json('Resources updated').catch((error) => {
          console.log(error.response.body)
          // console.log(error.response.body.errors[0].message)
        })
      } else {
        res.send('User does not exist')
      }
    })
    .catch(err => {
      res.send('error: ' + err)
    })
})

users.get('/send', (req, res) => {
  var decoded = jwt.verify(req.headers['authorization'], process.env.SECRET_KEY)
  especialid = decoded._id;
  especialcode = decoded.code;
  User.findOne({
    _id: decoded._id
  })
    .then(user => {
      if (user) {
        const sgMail = require('@sendgrid/mail');
        sgMail.setApiKey('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');
        const msg = {
          to: `noelarguello1999@gmail.com`,
          from: 'narguellos@est.utn.ac.cr',
          subject: 'Verify your account '+ `${user.firstName}`,
          text: 'use the code tu verify your account' + `${user.code}`,
          html: '<strong>use the this code: '+ `${user.code}` + ' to verify your account  </strong>',
        };
          sgMail.send(msg).then(() => {
            console.log('Message sent')
        }).catch((error) => {
            console.log(error.response.body)
            // console.log(error.response.body.errors[0].message)
        })
        res.json(user)
      } else {
        res.send('User does not exist')
      }
    })
    .catch(err => {
      res.send('error: ' + err)
    })
})

users.put('/update', (req, res) => {
  console.log(especialid)
  User.findOne({
    _id: especialid
  })
    .then(user => {
      if (user) {
        user.isVerified = true
        user.firstName= req.body.firstName
        user.lastName = req.body.lastName
        user.phone = req.body.phone
        user.country = req.body.country
        user.email = req.body.email
        user.save().then(()=>{
        res.status(200).json('Resources updated')
          .catch((error) => {
            console.log(error.response.body)
            // console.log(error.response.body.errors[0].message)
          })
        });
      } else {
        res.send('User does not exist')
      }
    })
    .catch(err => {
      res.send('error: ' + err)
    })
})

users.post('/register', (req, res) => {
  var password = '_' + Math.random().toString(36).substr(2, 9); //generate a random password
  const today = new Date()
  const userData = {
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    email: req.body.email,
    password: req.body.password,
    country: req.body.country,
    phone: req.body.phone,
    bornDate: req.body.bornDate,
    code: password,
    isVerified: req.body.isVerified,
    created: today
  }
  console.log(userData.code)

  User.findOne({
    email: req.body.email
  })
    //TODO bcrypt
    .then(user => {
      if (!user) {
        User.create(userData)
          .then(user => {
            const payload = {
              _id: user._id,
              firstName: user.firstName,
              lastName: user.lastName,
              email: user.email,
              phone: user.phone,
              country: user.country,
              code: user.code,
              bornDate: user.bornDate,
              isVerified: user.isVerified
            }
            let token = jwt.sign(payload, process.env.SECRET_KEY, {
              expiresIn: 1440
            })
            res.json({ token: token })
          })
          .catch(err => {
            res.send('error: ' + err)
          })
      } else {
        res.json({ error: 'User already exists' })
      }
    })
    .catch(err => {
      res.send('error: ' + err)
    })
})

users.post('/login', (req, res) => {
  User.findOne({
    email: req.body.email
  })
    .then(user => {
      if (user) {
        especialid= user._id
        const payload = {
          _id: user._id,
          firstName: user.firstName,
          lastName: user.lastName,
          email: user.email,
          isVerified: user.isVerified
        }
        let token = jwt.sign(payload, process.env.SECRET_KEY, {
          expiresIn: 1440
        })
        res.json({ token: token })
      } else {
        res.json({ error: 'User does not exist' })
      }
    })
    .catch(err => {
      res.send('error: ' + err)
    })
})

users.get('/profile', (req, res) => {
  var decoded = jwt.verify(req.headers['authorization'], process.env.SECRET_KEY)

  User.findOne({
    _id: decoded._id
  })
    .then(user => {
      if (user) {
        res.json(user)
      } else {
        res.send('User does not exist')
      }
    })
    .catch(err => {
      res.send('error: ' + err)
    })
})

module.exports = users