const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PlaylistSchema = Schema({
    email: {
        type: String,
    },
    username:{
        type: String
    },
    url:{
        type: String
    }
    
}, {timestamps: true}); 

module.exports = User = mongoose.model('playlists', PlaylistSchema)