const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ChildSchema = Schema({
    email: {
        type: String,
    },

    password: {
        type: String,
    },

    username: {
        type: String,
    },
    
}, {timestamps: true}); 

module.exports = User = mongoose.model('childs', ChildSchema)