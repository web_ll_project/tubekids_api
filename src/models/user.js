const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = Schema({
    email: {
        type: String,
        unique: true,
        required: 'Your email is required',
        trim: true
    },

    password: {
        type: String,
        required: 'Your password is required',
        max: 100,
        min:6
    },

    firstName: {
        type: String,
        required: 'First Name is required',
        max: 100
    },

    lastName: {
        type: String,
        required: 'Last Name is required',
        max: 100
    },
    phone:{
        type: Number,
        required: "Phone number is required",
        min:8
    },
    country:{
        type: String
    },
    bornDate:{
        type: Date,
        required: "Date is required"

    },
    code:{
        type: String
    },
    isVerified:{
        type: Boolean,
        default: false
    },
    
}, {timestamps: true}); 

module.exports = User = mongoose.model('users', UserSchema)